const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const appSettings = require("./app/appSettings");
const https = require('https');
const fs = require('fs');

const app = express();

var corsOptions = {
  origin: appSettings.CORS_ORIGIN
}; //setting

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./app/models");
const Role = db.role;
const Status = db.status;

db.mongoose
  .connect(`mongodb://${appSettings.DB_HOST}:${appSettings.DB_PORT}/${appSettings.DB_NAME}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
    initial();
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to JAM." });
});

app.get("/api/version", (req, res) => {
  res.json({ message: appSettings.VERSION });
});

// routes
require('./app/routes/auth.routes')(app);
require('./app/routes/user.routes')(app);
require('./app/routes/application.routes')(app);
require('./app/routes/status.routes')(app);

const PORT = appSettings.PORT; //setting
const IP = appSettings.IP; //setting

// const options = {
//   key: fs.readFileSync(appSettings.KEY_PATH),
//   cert: fs.readFileSync(appSettings.CERT_PATH)
// };

// app.use((req, res) => {
//   res.writeHead(200);
//   res.end("hello world\n");
// });

// https.createServer(app).listen(PORT, IP, () => {
// 	console.log(`Server is running on ${IP}:${PORT}.`);
//   });

app.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}.`);
});
  

function initial() {
  Role.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new Role({
        name: "user"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }
      });

      new Role({
        name: "admin"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }
      });
    }
  });

  Status.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new Status({
        name: "To do",
        order: 0
      }).save(err => {
        if (err) {
          console.log("error", err);
        }
      });

      new Status({
        name: "Application sent",
        order: 1
      }).save(err => {
        if (err) {
          console.log("error", err);
        }
      });

      new Status({
        name: "Interview set",
        order: 2
      }).save(err => {
        if (err) {
          console.log("error", err);
        }
      });

      new Status({
        name: "Offer received",
        order: 3
      }).save(err => {
        if (err) {
          console.log("error", err);
        }
      });

      new Status({
        name: "Refused",
        order: 4
      }).save(err => {
        if (err) {
          console.log("error", err);
        }
      });
    }
  });
}