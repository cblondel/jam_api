const mongoose = require("mongoose");
const applicationController = require("./application.controller");
const db = require("../models");
const User = db.user;
const Application = db.application;

exports.get = (req, res) => {
	let query = {};
	if (!req.body.deleted)
		query = {when_deleted: null};
	User.find(query).then((elements) => {
		res.status(200).send(elements);
	});
};

// todo : prevent update of important fields
exports.update = (req, res) => {
	try {
		User.updateOne(
			{_id:mongoose.Types.ObjectId(req.params.id)}, // query
			{	// update
				$set: {
					username: req.body.username,
					email: req.body.email
				}
			},
			{}, // options
			function(err, object) { // callback
				
				if (err){
					if (err.name === 'MongoError' && err.code === 11000) {
						const errField = err.message.split(" ").map((el, key, array) => el.includes("index:") && array[key + 1]).filter(Boolean)[0].replace(/\_\d+/g, "");
						const value = errField === "username" ? req.body.username : req.body.email;
						res.status(500).send({ message: `Failed! ${errField.charAt(0).toUpperCase() + errField.slice(1)} '${value}' is already in use!` });
						return;
					  }
					  
					res.status(500).send({ message: err.message });
					return;
				}
				res.status(200).send({ message: "User updated" });
			}
		);
	} catch (error) {
		res.status(500).send({ message: "An error occured, please contact an administrator"});
		return;
	}
};

exports.archive = (req, res) => {
  if (req.params.id == req.body.user_id) {
		res.status(500).send({ message: "You can not archive your own account"});
		return;
	}

	try {
		User.updateOne(
			{_id:mongoose.Types.ObjectId(req.params.id)}, // query
			{$set: {"when_deleted" : Date.now()}}, // update
			{}, // options
			function(err, object) { // callback
			  if (err){
				  res.status(500).send({ message: err.message });
					return;
			  }
			  if (!object.nModified){
				  res.status(500).send({ message: "User not found" });
					return;
			  }
			  res.status(200).send("User archived");
			}
		);
	} catch (error) {
		res.status(500).send({ message: "An error occured, please contact an administrator"});
		return;
	}
};

exports.delete = (req, res) => {
	Application.deleteMany(
		{
			user_id: mongoose.Types.ObjectId(req.params.id)
		},
		{}, // options
		function(err, object) { // callback
			if (err){
				res.status(500).send({ message: err.message });
				return;
		  	}
			
			User.deleteOne({
			  _id:mongoose.Types.ObjectId(req.params.id)
			}, // query
			{}, // options
			function(err, object) { // callback
				if (err){
					res.status(500).send({ message: err.message });
					return;
				}
				res.status(204).send({ message: "User deleted."	});
			});
		}
	)
  };

exports.restore = (req, res) => {
	try {
		User.updateOne(
			{_id:mongoose.Types.ObjectId(req.params.id)}, // query
			{$set: {"when_deleted" : null}}, // update
			{}, // options
			function(err, object) { // callback
				if (err){
					res.status(500).send({ message: err.message });
					  return;
				}
				if (!object.nModified){
					res.status(500).send({ message: "User not found" });
					  return;
				}
				res.status(200).send("User restored");
			}
		);
	} catch (error) {
		res.status(500).send({ message: "An error occured, please contact an administrator"});
		return;
	}
};