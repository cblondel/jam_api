const config = require("../config/auth.config");
const mailgun = require("../utils/mailer");
const validate = require('../utils/validation');
var crypto = require('crypto');
const db = require("../models");
const User = db.user;
const Role = db.role;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signup = (req, res) => {
  if (!validate.isPasswordValid(req.body.password) && req.body.password === req.body.confirmation) {
    res.status(400).send({ message: "Password must be at least 8 characters long and contains one number, one lowercase and one uppercase letter" });
    return;
  }

  const user = new User({
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
    when_created: Date.now(),
    when_deleted: null
  });

  user.save((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (req.body.roles) {
      Role.find(
        {
          name: { $in: req.body.roles }
        },
        (err, roles) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          user.roles = roles.map(role => role._id);
          user.save(err => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }
            
            mailgun.sendMail(user.email, "Welcome to JAM", "welcome", {
              "user": {
                  "username": user.username,
                  "email": user.email
              },
              "fromEmail": mailgun.contact
            })
            res.send({ message: "User was registered successfully!" });
          });
        }
      );
    } else {
      Role.findOne({ name: "user" }, (err, role) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        user.roles = [role._id];
        user.save(err => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          mailgun.sendMail(user.email, "Welcome to JAM", "welcome", {
            "user": {
                "username": user.username,
                "email": user.email
            },
            "fromEmail": mailgun.contact
          })
          res.send({ message: "User was registered successfully!" });
        });
      });
    }
  });
};

exports.signin = (req, res) => {
  User.findOne({
    username: req.body.username
  })
    .populate("roles", "-__v")
    .exec((err, user) => {
      if (err) {
        return res.status(500).send({
          accessToken: null,
          message: err
        });
      }

      if (!user) {
        return res.status(404).send({
          preventRedirect: true,
          accessToken: null,
          message: "User not found."
        });
      }

      if (user.when_deleted) {
        return res.status(403).send({
          preventRedirect: true,
          accessToken: null,
          message: "The user has been deleted, please contact an administrator."
        });
      }

      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(400).send({
          accessToken: null,
          message: "Invalid Password!"
        });
      }

      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 3600 // 1 hour
      });

      var authorities = [];

      for (let i = 0; i < user.roles.length; i++) {
        authorities.push("ROLE_" + user.roles[i].name.toUpperCase());
      }
      
      res.status(200).send({
        id: user._id,
        username: user.username,
        email: user.email,
        roles: authorities,
        accessToken: token
      });
    });
};

exports.forgotPassword = (req, res) => {
  User.findOne({
    email: req.body.email
  }).exec((err, user) => {
    if (err) {
      return res.status(500).send({ message: err });
    }
    
    if (!user) {
      return res.status(400).send({ message: "User not found." });
    }

    crypto.randomBytes(48, (err, buffer) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      } else {
        var token = buffer.toString('hex');
        const expires = Date.now() + 1200000;
        User.updateOne(
          {
            email: user.email
          },
          {
            $set: {
              resetPasswordToken : token,
              resetPasswordExpires : expires, // 20 minutes
            }
          }
        ).exec((err, response) => {
          if (err) {
            return res.status(500).send({ message: err });
          }
          
          if (!response) {
            return res.status(400).send({ message: "User not found." });
          }
          
          console.log("response " ,response)
          mailgun.sendMail(user.email, "Reset your password", "forgot_password", {
            "link": `${req.headers.origin}/reset/${token}`,
            "fromEmail": mailgun.contact
          });
          res.status(200).send({ message: `An email has been sent to ${user.email} with further instructions.`});
        })
      }
    });
  })
};

exports.resetPassword = (req, res) => {
  if (!validate.isPasswordValid(req.body.password) && req.body.password === req.body.confirmation) {
    res.status(400).send({ message: "Password must be at least 8 characters long and contains one number, one lowercase and one uppercase letter" });
    return;
  }

  User.findOne({ 
    resetPasswordToken: req.params.token,
    resetPasswordExpires: { $gt: Date.now() }
  }, function(err, user) {
    if (err) {
      return res.status(500).send({ message: err });
    }

    if (!user) {
      return res.status(400).send({message: 'Password reset token is invalid or has expired.'});
    }

    user.password = bcrypt.hashSync(req.body.password, 8);
    user.resetPasswordToken = undefined;
    user.resetPasswordExpires = undefined;

    user.save(err => {
      if (err) {
        return res.status(500).send({ message: err });
      }
    });
    
    mailgun.sendMail(user.email, "Your password has been changed", "reset_password", {
      "email": user.email,
      "fromEmail": mailgun.contact
    });
    return res.status(200).send({ message: "Password updated" });
  })
};

exports.updatePassword = (req, res) => {
  if (!validate.isPasswordValid(req.body.password) && req.body.password === req.body.confirmation) {
    res.status(400).send({ message: "Password must be at least 8 characters long and contains one number, one lowercase and one uppercase letter" });
    return;
  }
  
  User.findOne({ 
    _id: req.params.id,
  }, function(err, user) {
    if (err) {
      return res.status(500).send({ message: err });
    }

    if (!user) {
      return res.status(400).send({message: 'User not found.'});
    }

    user.password = bcrypt.hashSync(req.body.password, 8);

    user.save(err => {
      if (err) {
        return res.status(500).send({ message: err });
      }
    });
    
    mailgun.sendMail(user.email, "Your password has been changed", "reset_password", {
      "email": user.email,
      "fromEmail": mailgun.contact
    });
    
    return res.status(200).send({ message: "Password updated" });
  })
};
