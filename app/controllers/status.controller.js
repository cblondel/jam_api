const mongoose = require("mongoose");
const db = require("../models");
const Status = db.status;

exports.get = (req, res) => {
	Status.find().sort({ order: 1 }).then((elements) => {
		res.status(200).send(elements);
	});
};
