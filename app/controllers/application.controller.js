const mongoose = require("mongoose");
const db = require("../models");
const Application = db.application;

exports.get = (req, res) => {
	try {
		Application
		.aggregate([
			{ 
				$match: { 
					_id: mongoose.Types.ObjectId(req.params.id),
					user_id: mongoose.Types.ObjectId(req.params.user_id),
					when_deleted: null
				} 
			},
			{
				$lookup:
					{
						from: "status",
						localField: "status",
						foreignField: "_id",
						as: "status_item"
					}
			}
		])
		.then((element) => {
			if (!element) {
				return res.status(404).send({ message: "Application not found !"});
			}
			
			res.status(200).send(element.shift());
		});
	} catch (error) {
		return res.status(404).send(res, "Application not found", null);
	}
};

exports.getAll = (req, res) => {
	try {
		Application
		.aggregate([
			{ 
				$match: { 
					user_id: mongoose.Types.ObjectId(req.params.user_id), 
					when_deleted: null
				} 
			},
			{
				$lookup:
					{
						from: "status",
						localField: "status",
						foreignField: "_id",
						as: "status_item"
					}
			}
		])
		.then((elements) => {
			res.status(200).send(elements);
		});
	} catch (error) {
		res.status(500).send({ message: "An error occured, please contact an administrator."});
		return;
	}
};

exports.getNumberByStatus = (req, res) => {
	try {
		Application.countDocuments({ 
			user_id: mongoose.Types.ObjectId(req.params.user_id),
			status: mongoose.Types.ObjectId(req.params.status_id)
		})
		.then((number) => {
			res.status(200).send(`${number}`);
		});
	} catch (error) {
		return res.status(500).send({ message: "An error occured, please contact an administator."});
	}
};

exports.save = (req, res) => {
	const application = new Application({
		user_id: req.params.user_id ,
		company_name: req.body.companyName,
		job: req.body.job,
		job_description: req.body.jobDescription,
		website: req.body.website,
		notes: req.body.notes,
		location: req.body.location,
		contacts: req.body.contacts,
		status: req.body.status,
		when_created: Date.now(),
		when_updated: null,
		when_deleted: null
	});
  
	application.save((err, user) => {
		if (err) {
			res.status(500).send({ message: err });
			return;
	  	}
  
		res.status(200).send({ message: "Application was created successfully!" });
	});
};

// todo : prevent update of important fields
exports.update = (req, res) => {
	try {
		Application.updateOne(
			{	// query
				_id:mongoose.Types.ObjectId(req.params.id),
				user_id:mongoose.Types.ObjectId(req.body.userId)
			}, 
			{	// update
				$set: {
					company_name: req.body.companyName,
					job: req.body.job,
					job_description: req.body.jobDescription,
					website: req.body.website,
					notes: req.body.notes,
					location: req.body.location,
					contacts: req.body.contacts,
					status: mongoose.Types.ObjectId(req.body.status),
					when_updated: Date.now()
				}
			},
			{}, // options
			function(err, object) { // callback
				if (err){
				res.status(500).send({ message: err.message });
						return;
				}
				res.status(200).send({ message: "Application updated" });
			}
		);
	} catch (error) {
		res.status(500).send({ message: "An error occured, please contact an administrator."});
		return;
	}
};

exports.archive = (req, res) => {
	try {
		Application.updateOne(
			{
				_id:mongoose.Types.ObjectId(req.params.id),
				user_id:mongoose.Types.ObjectId(req.body.user_id)
			}, // query
			{
				$set: {"when_deleted" : Date.now()}
			}, // update
			{}, // options
			function(err, object) { // callback
			  if (err){
				  res.status(500).send({ message: err.message });
					return;
			  }
			  if (!object.nModified){
				  res.status(500).send({ message: "Application not found" });
					return;
			  }
			  res.status(200).send("Application archived");
			}
		  );
	} catch (error) {
		res.status(500).send({ message: "An error occured, please contact an administrator."});
		return;
	}
};

exports.delete = (req, res) => {
	try {
		Application.deleteOne(
			{
				_id:mongoose.Types.ObjectId(req.params.id),
				user_id:mongoose.Types.ObjectId(req.params.user_id)
			}, // query
			{}, // options
			function(err, object) { // callback
			  if (err){
				  res.status(500).send({ message: err.message });
					return;
			  }
			  res.status(200).send({ message: "Application deleted." });
			}
		  );
	} catch (error) {
		res.status(500).send({ message: "An error occured, please contact an administrator."});
		return;
	}
};

exports.restore = (req, res) => {
	try {
		Application.updateOne(
			{_id:mongoose.Types.ObjectId(req.params.id),user_id:mongoose.Types.ObjectId(req.body.user_id)}, // query
			{$set: {"when_deleted" : null}}, // update
			{}, // options
			function(err, object) { // callback
			  if (err){
				  res.status(500).send({ message: err.message });
					return;
			  }
			  if (!object.nModified){
				  res.status(500).send({ message: "Application not found" });
					return;
			  }
			  res.status(200).send("Application restored");
			}
		  );
	} catch (error) {
		res.status(500).send({ message: "An error occured, please contact an administrator."});
		return;
	}
};

exports.deleteAllFromUser = (req, res) => {
	Application.deleteMany(
		{
			user_id: mongoose.Types.ObjectId(req.params.id)
		},
		{}, // options
		function(err, object) { // callback
			if (err){
				res.status(500).send({ message: err.message });
				return;
		  	}
			
			res.status(204).send({ message: "Applications deleted." });
		})
};