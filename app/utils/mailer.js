const appSettings = require("../appSettings");

let API_KEY = appSettings.MAILGUN_API_KEY; //setting
let DOMAIN = appSettings.MAILGUN_DOMAIN; //setting
let mailgun = require("mailgun-js")({apiKey: API_KEY, domain: DOMAIN, host: "api.eu.mailgun.net"});

const email_sender = "no-reply@my-jam.tech";

exports.sendMail = function(email_reciever, email_subject, email_template, email_data){ 
	const data = { 
	"from": email_sender, 
	"to": email_reciever, 
	"subject": email_subject,
	"template": email_template,
	"h:X-Mailgun-Variables": JSON.stringify(email_data)
	}; 

	mailgun.messages().send(data, (error, body) => { 
		if(error) console.log(error);
	});
}

exports.contact = "contact@my-jam.tech"
