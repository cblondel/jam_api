const mongoose = require("mongoose");

const User = mongoose.model(
  "User",
  new mongoose.Schema({
    username: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    roles: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role"
      }
    ],
    when_created: mongoose.Schema.Types.Date,
    when_deleted: mongoose.Schema.Types.Date,
    resetPasswordToken: String,
    resetPasswordExpires: mongoose.Schema.Types.Date
  })
);

module.exports = User;