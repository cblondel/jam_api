const mongoose = require("mongoose");

const Application = mongoose.model(
  "Application",
  new mongoose.Schema({
    user_id: mongoose.Schema.Types.ObjectId,
    company_name: String,
    job: String,
    job_description: String,
    website: String,
    notes: String,
    location: String,
    contacts: [
      {
      name: String,
      job: String,
      email: String
      }
    ],
    status: mongoose.Schema.Types.ObjectId,
    when_created: mongoose.Schema.Types.Date,
    when_updated: mongoose.Schema.Types.Date,
    when_deleted: mongoose.Schema.Types.Date
  })
);

module.exports = Application;