const controller = require("../controllers/application.controller");
const { authJwt } = require("../middlewares");

module.exports = function (app) {
	app.use(function(req, res, next) {
	  res.header(
		"Access-Control-Allow-Headers",
		"x-access-token, Origin, Content-Type, Accept"
	  );
	  next();
	});
  
	app.get(
		"/api/application/:user_id/all",
		[authJwt.verifyToken],
		controller.getAll
	  );

	app.get(
		"/api/application/:user_id/:id",
		[authJwt.verifyToken],
		controller.get
	);

	app.post(
		"/api/application/:user_id/save",
		[authJwt.verifyToken],
		controller.save
	);

	app.put(
		"/api/application/update/:id",
		[authJwt.verifyToken],
		controller.update
	);

	app.delete(
		"/api/application/delete/:id/:user_id",
		[authJwt.verifyToken],
		controller.delete
	);

	app.put(
		"/api/application/restore/:id",
		[authJwt.verifyToken],
		controller.restore
	);
};