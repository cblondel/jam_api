const { authJwt } = require("../middlewares");
const controller = require("../controllers/user.controller");

module.exports = function (app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get(
    "/api/user/get/",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.get
  );
  
  app.put(
    "/api/user/update/:id",
    [authJwt.verifyToken],
    controller.update
  );

  app.put(
    "/api/user/archive/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.archive
  );

  app.delete(
    "/api/user/delete/:id",
    [authJwt.verifyToken],
    controller.delete
  );

  app.put(
    "/api/user/restore/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.restore
  );
};

// How to limit access
// app.get("/api/test/all", controller.allAccess);
// app.get("/api/test/user", [authJwt.verifyToken], controller.userBoard);
// app.get("/api/test/admin", [authJwt.verifyToken, authJwt.isAdmin], controller.adminBoard);
